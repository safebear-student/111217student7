package com.bdd.app;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "~@to-do",
        glue = "com.bdd.app",
        features = {"classpath:simplerisk.features/user_management.feature"}
)
public class RunCukesTest {
}
