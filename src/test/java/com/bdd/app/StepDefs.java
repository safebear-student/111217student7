package com.bdd.app;

import com.bdd.app.pages.ConfigurePage;
import com.bdd.app.pages.LoginPage;
import com.bdd.app.pages.ReportingPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class StepDefs {

    String url;
    WebDriver browser;
    LoginPage loginPage;
    ReportingPage reportingPage;
    ConfigurePage configurePage;

    @Before
    public void setUp(){
        browser = new ChromeDriver();
        url = "http://simplerisk.local/index.php";
        loginPage = new LoginPage(browser);
        configurePage = new ConfigurePage(browser);
        reportingPage = new ReportingPage(browser);

        browser.get(url);
        browser.manage().window().maximize();
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(){
        try{
            reportingPage.logout();
        } catch (Exception e) {
        }
        browser.quit();
    }

    @Given("^a user called (.+) with (.+) permissions and password (.+)$")
    public void a_user_is_created(String username, String permissions, String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        loginPage.enterUsername("admin");
        loginPage.enterPassword("^Y&U8i9o0p");
        loginPage.login();
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        if(configurePage.checkIfUserIsPresent(username)){
            configurePage.logout();
        }
        else{
            configurePage.enterNewUsername(username);
            configurePage.enterNewPassword(password);
            configurePage.givePermissions(permissions);
            configurePage.createUser();
            configurePage.logout();
        }
    }

    @When("^(.+) is logged with the password (.+)$")
    public void admin_is_logged_in(String username, String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.login();
    }

    @Then("^he is able to view (.+)'s account$")
    public void admin_is_able_to_view_users_account(String username) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        configurePage.viewUserDetails(username);
        assertTrue(configurePage.checkUserDetails(username));
    }


}
